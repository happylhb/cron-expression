<?php

namespace Cron;

use DateTime;


/**
 * Year field.  Allows: * , / -
 */
class YearField extends AbstractField
{
    protected $rangeStart = 2019;
    protected $rangeEnd = 2099;
    public function isSatisfiedBy(DateTime $date, $value)
    {
        return $this->isSatisfied($date->format('Y'), $value);
    }

    public function increment(DateTime $date, $invert = false, $parts = null)
    {
        if ($invert) {
            $date->modify('-1 year');
            $date->setDate($date->format('Y'), 12, 31);
            $date->setTime(23, 59, 59);
        } else {
            $date->modify('+1 year');
            $date->setDate($date->format('Y'), 1, 1);
            $date->setTime(0, 0, 0);
        }

        return $this;
    }

}
